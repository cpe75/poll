<?php
        class Home extends CI_Controller {
                public function __construct(){
                        parent::__construct();
                        $this->load->model('Poll_model');
                }
                
                public function index(){             
                        if(isset($_SESSION['username'])){
                                redirect('home/account');
                        }

                        $data['title'] = "Poll Website";    
                        $data['poll'] = $this->Poll_model->getPolls();
                        $data['poll_options'] = $this->Poll_model->getPoll_options();
                        $data['poll_votes'] = $this->Poll_model->getPoll_votes();

                        $this->load->view('templates/header', $data);
                        $this->load->view('pages/index', $data);
                        $this->load->view('templates/footer');                    
                }

                public function account(){
                        if(!isset($_SESSION['username'])){
                                redirect('home');
                        }

                        $data["title"] = "Welcome";
                        $data['poll'] = $this->Poll_model->getPolls();
                        $data['poll_options'] = $this->Poll_model->getPoll_options();
                        $data['poll_votes'] = $this->Poll_model->getPoll_votes();
                        $data['getid'] = $this->Poll_model->getMax();
                        
                        
                        
                        $this->load->view('templates/account_header', $data);
                        $this->load->view('pages/home', $data);
                        $this->load->view('pages/pollgraph');
                        $this->load->view('templates/footer');
                }

                public function voted(){
                        if(!isset($_SESSION['username'])){
                                redirect('home');
                        }

                        $data["title"] = "Welcome";
                        $data['poll'] = $this->Poll_model->getPolls();
                        $data['poll_options'] = $this->Poll_model->getPoll_options();
                        $data['poll_votes'] = $this->Poll_model->getPoll_votes();
                        $data['getid'] = $this->Poll_model->getMax();
                        $poll_option_id = $this->input->get('poll_option');
                        $poll_ID = $this->Poll_model->getPoll_ID($poll_option_id);
                        $has_voted = $this->Poll_model->getHas_voted($poll_ID);
                        $voted = $this->Poll_model->vote($poll_option_id, $poll_ID, $has_voted);
                        if($voted){
                                echo "success vote";
                        }else{
                                echo "not success vote";
                        }

                        $this->load->view('templates/account_header', $data);
                        $this->load->view('pages/home', $data);
                        $this->load->view('templates/footer');
                        redirect('home/account');
                }

                public function pollCreate(){
                        if(!isset($_SESSION['username'])){
                               redirect('home');
                        }
                        $poll_question = $this->input->get('poll_question');
                        $id = $this->input->get('id');
                        $id = $id + '1';
                         

                        $create = $this->Poll_model->addPoll($id, $poll_question);              
                        $data['poll'] = $this->Poll_model->getPolls();
                        $data['poll_options'] = $this->Poll_model->getPoll_options();
                        $data['poll_votes'] = $this->Poll_model->getPoll_votes();
                        $data["title"] = "Create Poll";
                        $this->load->view('templates/account_header', $data);
                        $this->load->view('pages/pollcreated', $data);
                        $this->load->view('templates/footer');

                }

                public function pollOptionCreate(){
                        if(!isset($_SESSION['username'])){
                               redirect('home');
                        }
                        $data['poll'] = $this->Poll_model->getPolls();
                        $data['poll_options'] = $this->Poll_model->getPoll_options();
                        $data['poll_votes'] = $this->Poll_model->getPoll_votes();
                        $data["title"] = "Create Poll";
                        
                        $name = $this->input->get('name');
                        $id = $this->input->get('id');
                        $id = $id + '1';
                        
                        //must get poll_option_id
                        
                        $poll_option_id = $this->Poll_model->addPoll_options($id, $name);
                        
                        $this->Poll_model->initVote($id, $poll_option_id);
                                               

                        $this->load->view('templates/account_header', $data);
                        $this->load->view('pages/pollcreated', $data);
                        $this->load->view('templates/footer');
                }  

                public function delete(){
                    $id = $this->input->get('id');    
                    $this->Poll_model->delete($id);
                    redirect('home');

                }

                public function logout(){
                        session_destroy();
                        redirect('home');
                }
       
}