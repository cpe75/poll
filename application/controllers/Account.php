<?php
        class Account extends CI_Controller {


                public function __construct(){
                        parent::__construct();
                        $this->load->model('Account_model');
                }
                        
                public function register(){
                        $username = $this->input->post('username');
                        $password = $this->input->post('password');
                        $password_verify = $this->input->post('password_verify');          
                        $register = $this->Account_model->createAccount($username, $password, $password_verify);
                        $getUser_ID = $this->Account_model->getUser_ID($username); 
                        if($register){
                                $login_session = array(
                                        'username' => $username,
                                        'id' => $getUser_ID,
                                        'logged_in' => TRUE
                                );
                                $this->session->set_userdata($login_session);
                                
                                redirect('home/account');
                        }else{
                                $error = "Username or password does not match or username already exists";

                        }

                        $data["title"] = "Register";
                        $data["error"] = $error;
                        $this->load->view('templates/login_register_header', $data);
                        $this->load->view('pages/register', $data);
                        $this->load->view('templates/footer');

                }

                public function login(){
                        $username = $this->input->post('username');
                        $password = $this->input->post('password');     
                        $login = $this->Account_model->login($username, $password);
                        $getUser_ID = $this->Account_model->getUser_ID($username);
                        if($login){
                                // set session and redirect to home page
                                $login_session = array(
                                        'username' => $username,
                                        'id' => $getUser_ID,
                                        'logged_in' => TRUE
                                );
                                $this->session->set_userdata($login_session);
                                
                                redirect('home/account');
                        }
                        else{
                                $error = "Password incorrect or username does not exist ";     
                        }  
                        $data["title"] = "Login";
                        $data["error"] = $error;
                        $this->load->view('templates/login_register_header', $data);
                        $this->load->view('pages/login', $data);
                        $this->load->view('templates/footer');
                        
                }
                
}
