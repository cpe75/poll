<?php
    class Account_model extends CI_Model{
        public function __construct(){
            $this->load->database();
        }

        public function getAccounts(){
            $sql = "Select * from users";
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        public function createAccount($username, $password, $password_verify){
            $data = array(
                'username' => $username,
                'password' => $password
            );
            $sql = "Select * from users where username = ?";
            $query = $this->db->query($sql, array($username));
            if(($query->row() != NULL) || ($password != $password_verify) || $username == ''){
                return false;
            }
            $this->db->insert('users', $data);
            return true;
            }  
        
        public function login($username, $password){
            $sql = "Select * from users where username = ? and password = ?";
            $query = $this->db->query($sql, array($username, $password));           
            if($query->row() == NULL){
                return false;
            }
            return true;
        }

        public function getUser_ID($username){
            $sql = "Select id from users where username = '$username'";
            $query = $this->db->query($sql);
            $row = $query->row_array();

            if (isset($row)){
                    return $row['id'];

            }
        }

    }
