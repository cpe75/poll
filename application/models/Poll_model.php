<?php
    class Poll_model extends CI_Model{
        public function __construct(){
            $this->load->database();
        }

        public function getPolls(){
            $sql = "Select * from polls";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
        public function getPoll_options(){
            $sql = "Select * from poll_options";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
        public function getPoll_votes(){
            $sql = "Select * from poll_votes";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
        public function addPoll($id, $poll_question){
            $data = array(
                'id' => $id,
                'user_id' => $_SESSION['id'],
                'subject' => $poll_question
            );
            $query = $this->db->insert('polls', $data);
        }
        public function getMax(){
            $this->db->select_max('id');
            $query = $this->db->get('polls');
            return $query->result_array();
        }
        public function addPoll_options($poll_id, $name){
            $data = array(
               'poll_id' => $poll_id,
               'user_id' => $_SESSION['id'],
               'name' => $name
            );
            $query = $this->db->insert('poll_options', $data); 
            return $this->db->insert_id();        
        }

        public function initVote($poll_id, $poll_option_id){
            $data = array(
                'poll_id' => $poll_id,
                'poll_option_id' => $poll_option_id,
                'user_id' => $_SESSION['id'],
                'vote_count' => 0,
            );
            $query = $this->db->insert('poll_votes', $data);
        }

       public function getPoll_ID($poll_option_id){
            $sql = "Select poll_id from poll_votes where poll_option_id = '$poll_option_id'";
            $query = $this->db->query($sql);
            $row = $query->row_array();

            if (isset($row)){
                    return $row['poll_id'];

            }
            
        }

        public function getHas_voted($poll_ID){
            $sess =  $_SESSION['id'];
            $sql = "Select has_voted from user_votes where poll_id = $poll_ID and user_id = $sess";
            $query = $this->db->query($sql);
            $row = $query->row_array();

            if (isset($row)){
                    return $row['has_voted'];

            }
            
        }
        public function vote($poll_option_id, $poll_ID, $has_voted){
            if($has_voted == 1){
                return false;
            }
            else{
                    $data = array(
                        'poll_id' => $poll_ID,
                        'user_id' => $_SESSION['id'],
                        'has_voted' => 1,
                    );
                    $query = $this->db->insert('user_votes', $data);


                    $sql = "UPDATE poll_votes SET vote_count = vote_count + 1 where poll_option_id = '$poll_option_id' ";
                    $query = $this->db->query($sql);
                    return true;
            }

        }

        public function delete($id){  
         $this->db->where("poll_id", $id);  
           $this->db->delete("user_votes");
            $this->db->where("poll_id", $id);  
           $this->db->delete("poll_votes"); 
            $this->db->where("poll_id", $id);  
           $this->db->delete("poll_options"); 
            $this->db->where("id", $id);  
           $this->db->delete("polls");

           return true;
      } 


    }