
<!--login modal  -->

    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url();?>account/login" method="post">
          <!--input username -->
            <label>Username</label>
            <div class="form-group">
            <input class="form-control" type="text" name="username">
            </div>
            <!--input password -->
            <label>Password</label>
            <div class="form-group">
            <input class="form-control" type="password" name="password">
            </div>
            <p class="text-danger"><?php echo $error ?></p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Login</button>        
      </div>
      </form>
    </div>
