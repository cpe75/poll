
    <?php
    // SELECT RESULTS FROM POLLS and store them into an array
    // Example result is shown below
        // $dataPoints = array(
        //     array("y" => 10, "label" => "Apple"),
        //     array("y" => 4, "label" => "Mango"),
        //     array("y" => 5, "label" => "Orange"),
        //     array("y" => 7, "label" => "Banana"),
        //     array("y" => 4, "label" => "Pineapple"),
        //     array("y" => 6, "label" => "Pears"),
        // );
    ?>
 
    <body>
        <div id="chartContainer"></div>
 
        <script type="text/javascript">
 
            $(function () {
                var chart = new CanvasJS.Chart("chartContainer", {  //variable name is unique for each chart
                    theme: "theme1",
                    animationEnabled: true,
                    title: {
                        text: "First Chart"
                    },
                    data: [
                    {
                        type: "column",                
                        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                    }
                    ]
                });
                chart.render();
            });
        </script>
	 	

    </body>