   
   
    <div class="jumbotron">
    <h2>Create Poll</h2>
    <p>Create polls for free! </p>
    <p><a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#createPoll">Create a Poll</a></p>
    </div>

<?php
  foreach($getid as $id):
    $id =  $id['id'];
    endforeach;

?>
<!--create poll modal  -->
<div class="modal" id="createPoll">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Create Poll</h4>
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url();?>home/pollcreate" method="get">
          <input type="hidden" name="id" value='<?php echo $id;?>'>

          <!--Create Poll - input question  -->
            <label>Enter your question</label>
            <div class="form-group">
            <input class="form-control input-lg" type="text" id="inputLarge" name="poll_question">
            </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Next</button>
      </div>
        </form>   
    </div>
  </div>
</div>


<br>
<!-- POLLS -->

<?php
 foreach($poll as $val): //Title
      echo "<div class='panel panel-primary'>";
      ?>
      <div class="panel-heading">
      <form action="<?php echo base_url()?>home/delete" method='get'>
      <?php echo $val['subject'];?>
      <?php 
        if($val['user_id'] == $_SESSION['id']){
          echo "<input type='hidden' name='id' value='$val[id]'>";
          echo "<input type='button' data-toggle='modal' data-target='#del$val[id]' value=' X ' class='btn btn-danger btn-xs pull-right'></div>";
        }
        else if($_SESSION['username'] == 'admin'){
          echo "<input type='hidden' name='id' value='$val[id]'>";
          echo "<input type='button' data-toggle='modal' data-target='#$val[id]' value=' X ' class='btn btn-danger btn-xs pull-right'></div>";
        }
        else{
          echo "<input type='button' value=' X ' class='btn btn-danger disabled btn-xs pull-right'></div>";
        }
      
      
      ?>

<!--del poll modal  -->
<div class="modal" id=<?php echo "del".$val['id']; ?>>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Poll</h4>
      </div>
      <div class="modal-body">
          Confirm Delete?
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Delete</button>
      </div>
        </form>   
    </div>
  </div>
</div>



      <?php
      echo "<div class='panel-body'>";
      
      echo "<div class='container'>";
      ?>

      <form action="<?php echo base_url();?>home/voted" method="get">
     <?php
      $count = 0;
      foreach($poll_options as $po): //poll options
        if($po['poll_id'] != $val['id']){}
        else{
          $count = $count + 1;
          echo " <p><input type='radio' name='poll_option' value=" . $po['id'] .">";
          echo '  '.$po['name'].'</p>'; 

        }
        foreach($poll_votes as $votes): //poll votes
            echo "<div class='modal' id='$val[id]'>";
            echo "<div class='modal-dialog'>";
            echo "<div class='modal-content'>";
            echo "<div class='modal-header'>";
            echo "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
            echo "<h4 class='modal-title'>Result</h4>";  
            echo "</div>";
            echo "<div class='modal-body'>";

  ?>
            <div class="container-fluuid">
            <table class="table  "> 
              <tbody>              
                  <td><b>Poll Option</b></td>
                  <td><b>Votes</b></td>

  <?php         
                foreach($poll_options as $po): //print poll votes - poll options
                echo "<tr >";
                if($po['poll_id'] != $val['id']){}
                else{
                  echo '<td>'.$po['name'].'</td>';
                    foreach($poll_votes as $votes): //print poll votes
                    if($votes['poll_option_id'] != $po['id']){}
                    else{
                       echo '<td>'.$votes['vote_count'].'</td>';


                      $dataPoints = array(
                          array("y" => $po['id'], "label" => $po['name']),
 
                      );
                    }
                    
                    endforeach;
                }
                echo "</tr>";
                endforeach; 

               
?>
                               
              </tbody>
            </table> 
            </div>
  <?php
            echo" </div>";
            echo "<div class='modal-footer'>";
            echo "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";     
        endforeach;    
      endforeach;
      echo "<button type='submit' class='btn btn-success'>Vote</button>   
      <button type='button' class='btn btn-info' data-toggle='modal' data-target='#$val[id]' >View Results</button>";  
      echo "</form>";
      echo "</div></div></div>";
      echo "<br>";
 endforeach;
?>


