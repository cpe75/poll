
<html>
    <head>
        <title><?php echo $title;?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">    
         <script src="<?php echo base_url('assets/css/jquery.min.js');?>"></script>  
         <script src="<?php echo base_url('assets/css/bootstrap.min.js');?>"></script>
         <script src="<?php echo base_url('assets/css/canvasjs.min.js');?>"></script>

         <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/pics/favicon.ico');?>">
    </head>

    <body>
<!-- <div class="container-fluid blok2"> -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(''); ?>">PollCenter <span class="sr-only">Poll Website</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url(''); ?>">Welcome,  <?php echo $_SESSION['username']; ?> </a></li> 
        <li><a href="<?php echo base_url(''); ?>">Home <span class="sr-only">(current)</span></a></li>       
        <li><a href="<?php echo base_url().'/home/logout'; ?>">Logout</a></li>
        
      </ul>
 

    </div>
  </div>
</nav>

<div class="container">
