<html>
    <head>
        <title><?php echo $title;?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">    
         <script src="<?php echo base_url('assets/css/jquery.min.js');?>"></script>  
         <script src="<?php echo base_url('assets/css/bootstrap.min.js');?>"></script>
    </head>

    <body>
<!-- <div class="container-fluid blok2"> -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(''); ?>">PollCenter <span class="sr-only">Poll Website</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li <?php if($title=="Home"){echo "class='active'";} ?>><a href="<?php echo base_url(''); ?>">Home <span class="sr-only">(current)</span></a></li>       
        <li><a href="#" data-toggle="modal" data-target="#login">Login</a></li>
        <li><a href="#" data-toggle="modal" data-target="#register">Register</a></li>
        
      </ul>
 

    </div>
  </div>
</nav>

<div class="container">
